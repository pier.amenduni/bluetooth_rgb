/* Riceve messaggi che vengono inviati via Bluetooth HC-05 da una app. 
*/

#include <SoftwareSerial.h>

const int RXPin = 2;  // da collegare su TX di HC05
const int TXPin = 3;  // da collegare su RX di HC05
int pRosso = 11;
int pVerde = 10;
int pBlu = 9;

const int ritardo = 10;
//creo una nuova porta seriale via software
SoftwareSerial myBT = SoftwareSerial(RXPin, TXPin);
  
char msgChar;
  void colore(int rosso,int verde,int blu){
    analogWrite(pRosso, rosso); //SERVE PER COLLEGARE IL LED RGB
    analogWrite(pVerde, verde);
    analogWrite(pBlu, blu);
  }
void setup()
{
  pinMode(RXPin, INPUT);  //metto il pin in ascolto
  pinMode(TXPin, OUTPUT); //metto il pin in ascolto 
  pinMode(pRosso, OUTPUT);
  pinMode(pVerde, OUTPUT);
  pinMode(pBlu, OUTPUT);
    
  myBT.begin(38400);

  Serial.begin(9600); 
}

void loop() 
{
  String rosso="";//variabile che contene le stringhe spezzate
  String verde="";//variabile che contene le stringhe spezzate
  String blu="";//variabile che contene le stringhe spezzate
  int i=0;  //conntatore che serve per controllare quando arrivano dati
  int red=0; //variabile intera che contene lintero delle stringhe 
  int blue=0;  //variabile intera che contene lintero delle stringhe 
  int green=0;  //variabile intera che contene lintero delle stringhe 
  String str="";  //stringa vuota in cui ricevo la stringa dall'applicazione

  
  while(myBT.available()){  //mentre trasmette
    msgChar = char(myBT.read());
    Serial.print(msgChar);
    str+=msgChar;  // conacateno ogn carattere che mi viene inviato a msgChar
    i++;
    delay(ritardo);
    }

    
    if(i!=0){  //controllo per vedere se ricevo caratteri
    rosso=str.substring(0,3); //divido l'intera stringa
    verde=str.substring(3,6); //divido l'intera stringa 
    blu=str.substring(6,9); //divido l'intera stringa 

    
    //converto le seguenti stringhe in interi
    red=rosso.toInt();
    green=verde.toInt();
    blue=blu.toInt();

    //coloro il led
    colore(red,green,blue);
    }
}
