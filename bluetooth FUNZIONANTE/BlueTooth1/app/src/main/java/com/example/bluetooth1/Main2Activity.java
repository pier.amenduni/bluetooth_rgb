package com.example.bluetooth1;

import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import java.io.IOException;
import java.io.OutputStreamWriter;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;

import android.widget.LinearLayout;
import android.widget.SeekBar;

import java.io.OutputStreamWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
public class Main2Activity extends AppCompatActivity {
    LinearLayout llCerchioRGB;
    SeekBar sbTrasparenzaRed;
    SeekBar SbTrasparenzaGreen;
    SeekBar SbTrasparenzaBlue;
    int red = 0;
    int green = 0;
    int blue = 0;
    String colore = "";
    String c;
    String c1;
    String c2;

    public static String listElement;
    public static BluetoothSocket btSocket;
    public static OutputStreamWriter writer;
    public static BluetoothAdapter bluetoothAdapter;
    public static BluetoothDevice targetDevice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        llCerchioRGB = (LinearLayout) findViewById(R.id.llCerchioRGB);
        sbTrasparenzaRed = (SeekBar) findViewById(R.id.sbTrasparenzaRed);
        sbTrasparenzaRed.setOnSeekBarChangeListener(seekBarRed);
        SbTrasparenzaGreen = (SeekBar) findViewById(R.id.sbTrasparenzaGreen);
        SbTrasparenzaGreen.setOnSeekBarChangeListener(seekBarGreen);
        SbTrasparenzaBlue = (SeekBar) findViewById(R.id.sbTrasparenzaBlue);
        SbTrasparenzaBlue.setOnSeekBarChangeListener(seekBarBlue);
    }

    ;
    private SeekBar.OnSeekBarChangeListener seekBarRed = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
            GradientDrawable myCircle = (GradientDrawable) llCerchioRGB.getBackground();
                mostraColori(myCircle);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            try {
                send();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };

    private SeekBar.OnSeekBarChangeListener seekBarGreen = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
            GradientDrawable myCircle = (GradientDrawable) llCerchioRGB.getBackground();
                mostraColori(myCircle);
        }


        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            try {
                send();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };
    private SeekBar.OnSeekBarChangeListener seekBarBlue = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
            GradientDrawable myCircle = (GradientDrawable) llCerchioRGB.getBackground();
            mostraColori(myCircle);
        }


        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            try {
                send();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };

    public void mostraColori(GradientDrawable myCircle) {
        int color = Color.rgb(sbTrasparenzaRed.getProgress(), SbTrasparenzaGreen.getProgress(), SbTrasparenzaBlue.getProgress());
        myCircle.setColor(color);
    }

    public void send() throws IOException {
        c = Integer.toString(sbTrasparenzaRed.getProgress());
        c1 = Integer.toString(SbTrasparenzaGreen.getProgress());
        c2 = Integer.toString(SbTrasparenzaBlue.getProgress());

        if (sbTrasparenzaRed.getProgress() < 100) {
            c = "0" + c;
            if (sbTrasparenzaRed.getProgress() < 10) {
                c = "0" + c;
            }
        }
        if (SbTrasparenzaGreen.getProgress() < 100) {
            c1 = "0" + c1;
            if (SbTrasparenzaGreen.getProgress() < 10) {
                c1 = "0" + c1;
            }
        }

        if (SbTrasparenzaBlue.getProgress() < 100) {
            c2 = "0" + c2;
            if (SbTrasparenzaBlue.getProgress() < 10) {
                c2 = "0" + c2;
            }
        }
        colore = c + c1 + c2 + "\r\n";
        writer.write(colore);
        writer.flush();
    }


};
